package ru.mxjk.facebook.components;

import com.codeborne.selenide.ElementsContainer;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static ru.mxjk.facebook.utils.ElementUtils.elementsCollection;

public class LoginForm extends ElementsContainer {

    public final SelenideElement loginInput = $("[data-testid = 'royal_email']");
    public final SelenideElement passwordInput = $("[data-testid = 'royal_pass']");
    public final SelenideElement loginButton = $("[data-testid = 'royal_login_button']");
    public final SelenideElement signUpButton = $("[data-testid = 'open-registration-form-button']");

    public LoginForm shouldBeLoaded() {
        elementsCollection(loginInput, passwordInput, loginButton, signUpButton).exclude(visible).shouldBe(empty);
        return this;
    }

}
