package ru.mxjk.facebook.components;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.ElementsContainer;
import com.codeborne.selenide.SelenideElement;
import io.vavr.Lazy;
import ru.mxjk.facebook.constants.Gender;
import ru.mxjk.facebook.model.User;

import java.time.LocalDate;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.Condition.hidden;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static java.lang.String.format;
import static java.util.Optional.ofNullable;
import static ru.mxjk.facebook.utils.ElementUtils.elementsCollection;

public class SignUpForm extends ElementsContainer {

    public final SelenideElement registrationError = $("#reg_error");

    public final SelenideElement firstNameInput = $("[name = 'firstname']");
    public final SelenideElement firstNameErrorIcon = getInputErrorIcon(firstNameInput);
    public final SelenideElement firstNameErrorTip = getInputErrorTip(firstNameInput);

    public final SelenideElement lastNameInput = $("[name = 'lastname']");
    public final SelenideElement lastNameErrorIcon = getInputErrorIcon(lastNameInput);
    public final SelenideElement lastNameErrorTip = getInputErrorTip(lastNameInput);

    public final SelenideElement loginInput = $("[name = 'reg_email__']");
    public final SelenideElement loginErrorIcon = getInputErrorIcon(loginInput);
    public final SelenideElement loginErrorTip = getInputErrorTip(loginInput);

    public final SelenideElement loginConfirmInput = $("[name = 'reg_email_confirmation__']");
    public final SelenideElement loginConfirmErrorIcon = getInputErrorIcon(loginConfirmInput);
    public final SelenideElement loginConfirmErrorTip = getInputErrorTip(loginConfirmInput);

    public final SelenideElement passwordInput = $("#password_step_input");
    public final SelenideElement passwordErrorIcon = getInputErrorIcon(passwordInput);
    public final SelenideElement passwordErrorTip = getInputErrorTip(passwordInput);

    public final SelenideElement daySelect = $("#day");
    public final SelenideElement monthSelect = $("#month");
    public final SelenideElement yearSelect = $("#year");
    public final SelenideElement birthDayErrorIcon = $x("descendant::*[@data-name = 'birthday_wrapper']/following-sibling::i[1]");
    public final SelenideElement birthDayErrorTip = getInputErrorTip(daySelect);

    public final SelenideElement genderRadio = $("input[type = 'radio'][name = 'sex']");
    public final SelenideElement genderErrorIcon = $x("descendant::*[@data-name = 'gender_wrapper']/following-sibling::i[1]");
    public final SelenideElement genderErrorTip = getInputErrorTip(genderRadio);

    public final SelenideElement customGenderPronounSelect = $("[name = 'preferred_pronoun']");
    public final SelenideElement customGenderPronounErrorIcon = customGenderPronounSelect.$x("following-sibling::i[1]");
    public final SelenideElement customGenderPronounErrorTip = getInputErrorTip(customGenderPronounSelect);

    public final SelenideElement customGenderInput = $("[name = 'custom_gender']");

    public final SelenideElement registerButton = $("[name = 'websubmit']");

    public SignUpForm fillData(User user) {
        System.out.println(user.toString());
        ofNullable(user.getFirstName()).ifPresent(firstNameInput::setValue);
        ofNullable(user.getLastName()).ifPresent(lastNameInput::setValue);
        ofNullable(user.getLogin()).ifPresent(loginInput::setValue);
        ofNullable(user.getPassword()).ifPresent(passwordInput::setValue);
        fillBirthDate(user.getBirthDate());
        ofNullable(user.getGender()).map(Gender::getValueOnUI).ifPresent(genderRadio::selectRadio);
        return this;
    }

    public SignUpForm fillBirthDate(LocalDate birthDate) {
        if (birthDate != null) {
            daySelect.selectOption(String.valueOf(birthDate.getDayOfMonth()));
            monthSelect.selectOptionByValue(String.valueOf(birthDate.getMonthValue()));
            yearSelect.selectOption(String.valueOf(birthDate.getYear()));
        }
        return this;
    }

    public ElementsCollection primaryElements() {
        return elementsCollection(
                firstNameInput, lastNameInput, loginInput, passwordInput, daySelect, monthSelect, yearSelect, genderRadio
        );
    }

    public ElementsCollection inputErrors() {
        return elementsCollection(
                firstNameErrorIcon, firstNameErrorTip, lastNameErrorIcon, lastNameErrorTip, loginErrorIcon, loginErrorTip,
                loginConfirmErrorIcon, loginConfirmErrorTip, passwordErrorIcon, passwordErrorTip, birthDayErrorIcon,
                birthDayErrorTip, genderErrorIcon, genderErrorTip, customGenderPronounErrorIcon, customGenderPronounErrorTip
        );
    }

    public SignUpForm shouldNotHaveInputErrors() {
        inputErrors().exclude(hidden).shouldBe(empty);
        return this;
    }

    public SignUpForm shouldBeLoaded() {
        primaryElements().exclude(visible).shouldBe(empty);
        return this;
    }

    private static SelenideElement getInputErrorIcon(SelenideElement input) {
        return input.$x("ancestor::div[@id]/i[1]");
    }

    private static SelenideElement getInputErrorTip(SelenideElement input) {
        return Lazy.val(
                () -> $(format("[data-ownerid = '%s'] :first-child", input.getAttribute("id"))),
                SelenideElement.class
        );
    }

}
