package ru.mxjk.facebook.pages;

import lombok.NoArgsConstructor;
import org.openqa.selenium.support.FindBy;
import ru.mxjk.facebook.components.LoginForm;
import ru.mxjk.facebook.components.SignUpForm;

import static com.codeborne.selenide.Condition.enabled;
import static ru.mxjk.facebook.constants.Constants.DEFAULT_WAIT_TIMEOUT;

@NoArgsConstructor
public class LoginPage {

    @FindBy(css = "[data-testid = 'royal_login_form']")
    public LoginForm loginForm;

    @FindBy(id = "reg_box")
    public SignUpForm signUpForm;

    public SignUpForm openSignUpForm() {
        loginForm.shouldBeLoaded().signUpButton.waitUntil(enabled, DEFAULT_WAIT_TIMEOUT).click();
        return signUpForm.shouldBeLoaded();
    }

}
