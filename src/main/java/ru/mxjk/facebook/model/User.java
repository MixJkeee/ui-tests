package ru.mxjk.facebook.model;

import lombok.*;
import lombok.experimental.Accessors;
import ru.mxjk.facebook.constants.Gender;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class User {

    private String firstName;
    private String lastName;
    private String login;
    private String password;
    private LocalDate birthDate;
    private Gender gender;

}
