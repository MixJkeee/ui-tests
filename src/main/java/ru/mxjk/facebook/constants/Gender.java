package ru.mxjk.facebook.constants;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Optional;
import java.util.stream.Stream;

@Getter
@RequiredArgsConstructor
public enum Gender {
    FEMALE("1"),
    MALE("2"),
    OTHER("-1");

    private final String valueOnUI;

    public static Optional<Gender> findByOrdinal(int ordinal) {
        return Stream.of(values()).filter(gender -> gender.ordinal() == ordinal).findFirst();
    }
}
