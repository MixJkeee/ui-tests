package ru.mxjk.facebook.utils;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.WebDriverRunner.driver;
import static java.util.Arrays.asList;

public class ElementUtils {

    public static ElementsCollection elementsCollection(SelenideElement... elements) {
        return new ElementsCollection(driver(), asList(elements));
    }
}
