package ru.mxjk.facebook.utils;

import com.github.javafaker.Faker;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;
import lombok.Getter;
import ru.mxjk.facebook.constants.Gender;
import ru.mxjk.facebook.model.User;

import java.time.LocalDate;
import java.util.Locale;

@Getter
public class DataGenerator {

    private static final Locale DEFAULT_LOCALE = new Locale("en-US");
    private static final int DEFAULT_MIN_AGE = 5;
    private static final int DEFAULT_MAX_AGE = 100;
    private static final int MIN_VALID_PASSWORD_LENGTH = 6;
    private static final int DEFAULT_MAX_PASSWORD_LENGTH = 100;

    private final Faker faker;
    private final FakeValuesService fakeValuesService;

    public DataGenerator() {
        this.faker = new Faker(DEFAULT_LOCALE);
        this.fakeValuesService = new FakeValuesService(DEFAULT_LOCALE, new RandomService());
    }

    public DataGenerator(Locale locale) {
        this.faker = locale == null ? new Faker() : new Faker(locale);
        this.fakeValuesService = locale == null
                ? new FakeValuesService(DEFAULT_LOCALE, new RandomService())
                : new FakeValuesService(locale, new RandomService());
    }

    public User generateValidUserWithEmail() {
        var name = faker.name();
        return new User()
                .setFirstName(name.firstName())
                .setLastName(name.lastName())
                .setLogin(generateEmail())
                .setPassword(generateValidPassword())
                .setBirthDate(generateBirthDate())
                .setGender(getAnyGender(true));
    }

    public Gender getAnyGender(boolean onlyTraditional) {
        var ordinal = faker.number().numberBetween(0, onlyTraditional ? 2 : 3);
        return Gender.findByOrdinal(ordinal).orElseThrow();
    }

    public String generatePassword(int minLength, int maxLength) {
        return faker.lorem().characters(minLength, maxLength, true);
    }

    public String generateValidPassword() {
        return generatePassword(MIN_VALID_PASSWORD_LENGTH, DEFAULT_MAX_PASSWORD_LENGTH);
    }

    public LocalDate generateBirthDate(int minAge, int maxAge) {
        var age = faker.number().numberBetween(minAge, maxAge);
        var ageShiftInDays = faker.number().numberBetween(1L, 364L);
        return LocalDate.now().minusYears(age).minusDays(ageShiftInDays);
    }

    public LocalDate generateBirthDate() {
        return generateBirthDate(DEFAULT_MIN_AGE, DEFAULT_MAX_AGE);
    }

    public String generateEmail(Locale locale) {
        var fakeValuesService = new FakeValuesService(locale, new RandomService());
        return email(fakeValuesService);
    }

    public String generateEmail() {
        return email(fakeValuesService);
    }

    public String generatePhoneNumber() {
        return faker.phoneNumber().phoneNumber();
    }

    private static String email(FakeValuesService fakeValuesService) {
        return fakeValuesService.bothify("??????###@gmail.com");
    }

}
