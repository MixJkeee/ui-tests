package ru.mxjk.facebook;

import com.codeborne.selenide.Configuration;
import io.qameta.allure.Severity;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import ru.mxjk.facebook.components.SignUpForm;
import ru.mxjk.facebook.model.User;
import ru.mxjk.facebook.pages.LoginPage;
import ru.mxjk.facebook.utils.DataGenerator;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.closeWebDriver;
import static com.codeborne.selenide.logevents.SelenideLogger.addListener;
import static io.qameta.allure.SeverityLevel.CRITICAL;
import static ru.mxjk.facebook.constants.Constants.DEFAULT_WAIT_TIMEOUT;

@Severity(CRITICAL)
public class SignUpTest {

    private static final String URL = "http://www.facebook.com";

    private SignUpForm signUpForm;

    @BeforeAll
    public static void setUpAllure() {
        addListener("allure", new AllureSelenide());
    }

    @BeforeEach
    void setUp() {
        Configuration.startMaximized = true;
        signUpForm = open(URL, LoginPage.class).openSignUpForm();
    }

    @AfterEach
    void tearDown() {
        closeWebDriver();
    }

    @ParameterizedTest
    @DisplayName("User can register with email")
    @MethodSource("ru.mxjk.facebook.SignUpTestParams#validUsersWithEmail")
    void registerValidUserWithEmail(User user) {
        signUpForm.fillData(user)
                .loginConfirmInput.shouldBe(and("visible and empty", visible, empty))
                .setValue(user.getLogin());
        sleep(3000);
        signUpForm.registerButton
                .waitUntil(enabled, DEFAULT_WAIT_TIMEOUT).click();
        signUpForm.registerButton.shouldBe(disabled);
        signUpForm.shouldNotHaveInputErrors()
                .registerButton.waitUntil(enabled, DEFAULT_WAIT_TIMEOUT);
        signUpForm.registrationError.shouldBe(hidden);
        signUpForm.getSelf().waitUntil(hidden, 20000);
    }

    @ParameterizedTest
    @DisplayName("User can register with phone number")
    @MethodSource("ru.mxjk.facebook.SignUpTestParams#validUsersWithPhone")
    void registerUserWithPhoneNumber(User user) {
        signUpForm.fillData(user).loginConfirmInput.shouldBe(hidden);
        sleep(3000);

        signUpForm.registerButton
                .waitUntil(enabled, DEFAULT_WAIT_TIMEOUT).click();
        signUpForm.registerButton.shouldBe(disabled);
        signUpForm.shouldNotHaveInputErrors()
                .registerButton.waitUntil(enabled, DEFAULT_WAIT_TIMEOUT);
        signUpForm.registrationError.shouldBe(hidden);
        signUpForm.getSelf().waitUntil(hidden, 20000);
    }

    @ParameterizedTest
    @DisplayName("User with custom gender can register")
    @MethodSource("ru.mxjk.facebook.SignUpTestParams#validUsersWithCustomGender")
    void registerUserWithCustomGender(User userWithCustomGender, String customGenderPronounValue, String customGender) {
        signUpForm.fillData(userWithCustomGender)
                .loginConfirmInput.setValue(userWithCustomGender.getLogin());
        signUpForm.customGenderPronounSelect.selectOptionByValue(customGenderPronounValue);
        signUpForm.customGenderInput.setValue(customGender);
        sleep(3000);

        signUpForm.registerButton
                .waitUntil(enabled, DEFAULT_WAIT_TIMEOUT).click();
        signUpForm.registerButton.shouldBe(disabled);
        signUpForm.shouldNotHaveInputErrors()
                .registerButton.waitUntil(enabled, DEFAULT_WAIT_TIMEOUT);
        signUpForm.registrationError.shouldBe(hidden);
        signUpForm.getSelf().waitUntil(hidden, 20000);
    }

    @ParameterizedTest
    @DisplayName("User can not register without filling first name")
    @MethodSource("ru.mxjk.facebook.SignUpTestParams#usersWithoutFirstName")
    void canNotRegisterUserWithoutFirstName(User user) {
        signUpForm.fillData(user).loginConfirmInput.setValue(user.getLogin());

        signUpForm.firstNameErrorIcon.shouldBe(visible);
        signUpForm.lastNameErrorIcon.shouldBe(hidden);
        signUpForm.loginErrorIcon.shouldBe(hidden);
        signUpForm.loginConfirmErrorIcon.shouldBe(hidden);
        signUpForm.birthDayErrorIcon.shouldBe(hidden);
        signUpForm.genderErrorIcon.shouldBe(hidden);
        signUpForm.registrationError.shouldBe(hidden);
        signUpForm.registerButton.click();

        signUpForm.registerButton.shouldBe(visible).shouldBe(enabled);
        signUpForm.firstNameErrorTip.shouldBe(visible);
        signUpForm.getSelf().shouldBe(visible);
    }

    @ParameterizedTest
    @DisplayName("User can not register without filling last name")
    @MethodSource("ru.mxjk.facebook.SignUpTestParams#usersWithoutLastName")
    void canNotRegisterUserWithoutLastName(User user) {
        signUpForm.lastNameInput.click();
        signUpForm.fillData(user);

        signUpForm.firstNameErrorIcon.shouldBe(hidden);
        signUpForm.lastNameErrorIcon.shouldBe(visible);
        signUpForm.loginErrorIcon.shouldBe(hidden);
        signUpForm.loginConfirmErrorIcon.shouldBe(hidden);
        signUpForm.birthDayErrorIcon.shouldBe(hidden);
        signUpForm.genderErrorIcon.shouldBe(hidden);
        signUpForm.registrationError.shouldBe(hidden);

        signUpForm.registerButton.click();

        signUpForm.registerButton.shouldBe(visible).shouldBe(enabled);
        signUpForm.registrationError.shouldBe(hidden);
        signUpForm.lastNameErrorTip.shouldBe(visible);
        signUpForm.getSelf().shouldBe(visible);
    }

    @Test
    @DisplayName("User can not register without filling login")
    void canNotRegisterUserWithoutLogin() {
        var user = new DataGenerator().generateValidUserWithEmail().setLogin("  ");
        signUpForm.fillData(user);

        signUpForm.firstNameErrorIcon.shouldBe(hidden);
        signUpForm.lastNameErrorIcon.shouldBe(hidden);
        signUpForm.loginErrorIcon.shouldBe(visible);
        signUpForm.loginConfirmErrorIcon.shouldBe(hidden);
        signUpForm.birthDayErrorIcon.shouldBe(hidden);
        signUpForm.genderErrorIcon.shouldBe(hidden);
        signUpForm.registrationError.shouldBe(hidden);

        signUpForm.registerButton.click();

        signUpForm.registerButton.shouldBe(visible).shouldBe(enabled);
        signUpForm.getSelf().shouldBe(visible);
    }

    @Test
    @DisplayName("User can not register without login confirmation")
    void canNotRegisterUserWithoutLoginConfirmation() {
        var user = new DataGenerator().generateValidUserWithEmail();
        signUpForm.fillData(user);
        signUpForm.loginConfirmInput.click();
        signUpForm.lastNameInput.click();

        signUpForm.firstNameErrorIcon.shouldBe(hidden);
        signUpForm.lastNameErrorIcon.shouldBe(hidden);
        signUpForm.loginErrorIcon.shouldBe(hidden);
        signUpForm.loginConfirmErrorIcon.shouldBe(visible);
        signUpForm.birthDayErrorIcon.shouldBe(hidden);
        signUpForm.genderErrorIcon.shouldBe(hidden);
        signUpForm.registrationError.shouldBe(hidden);

        signUpForm.registerButton.click();

        signUpForm.registerButton.shouldBe(visible).shouldBe(enabled);
        signUpForm.getSelf().shouldBe(visible);
    }

}
