package ru.mxjk.facebook;

import org.junit.jupiter.params.provider.Arguments;
import ru.mxjk.facebook.utils.DataGenerator;

import java.util.Locale;
import java.util.stream.Stream;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static ru.mxjk.facebook.constants.Gender.OTHER;

public class SignUpTestParams {

    private static final DataGenerator rurDataGenerator = new DataGenerator(new Locale("ru-RU"));
    private static final DataGenerator enDataGenerator = new DataGenerator(new Locale("en-US"));

    private static final String VALID_GB_PHONE_NUMBER = "+447520640289";

    public static Stream<Arguments> validUsersWithEmail() {
        return Stream.of(
                Arguments.of(enDataGenerator.generateValidUserWithEmail()),
                Arguments.of(rurDataGenerator.generateValidUserWithEmail())
        );
    }

    public static Stream<Arguments> validUsersWithPhone() {
        return Stream.of(
                Arguments.of(rurDataGenerator.generateValidUserWithEmail().setLogin(rurDataGenerator.generatePhoneNumber())),
                Arguments.of(enDataGenerator.generateValidUserWithEmail().setLogin(VALID_GB_PHONE_NUMBER))
        );
    }

    public static Stream<Arguments> usersWithoutFirstName() {
        return Stream.of(
                Arguments.of(enDataGenerator.generateValidUserWithEmail().setFirstName(EMPTY)),
                Arguments.of(enDataGenerator.generateValidUserWithEmail().setFirstName("      "))
        );
    }

    public static Stream<Arguments> usersWithoutLastName() {
        return Stream.of(
                Arguments.of(
                        rurDataGenerator.generateValidUserWithEmail()
                                .setLogin(rurDataGenerator.generatePhoneNumber())
                                .setLastName(EMPTY)
                ),
                Arguments.of(
                        rurDataGenerator.generateValidUserWithEmail()
                                .setLogin(rurDataGenerator.generatePhoneNumber())
                                .setLastName("      ")
                )
        );
    }

    public static Stream<Arguments> validUsersWithCustomGender() {
        return Stream.of(
                Arguments.of(rurDataGenerator.generateValidUserWithEmail().setGender(OTHER), "1", EMPTY),
                Arguments.of(enDataGenerator.generateValidUserWithEmail().setGender(OTHER), "6", "Transgender")
        );
    }

}
